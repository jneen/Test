=begin
require 'sinatra'
require 'net/http'
require 'json'
require 'yaml'
require 'erb'
require 'net/ftp'
get '/hello/:name' do
  # matches "GET /hello/foo" and "GET /hello/bar"                                                                                                                                                              
  # params['name'] is 'foo' or 'bar'                                                                                                                                                                           
  #"Hello #{params['name']}!"                                                                                                                                                                                  
  url = "https://run.company.com/test.php?url=https://test.company.com"
  begin
    resp = Net::HTTP.get_response(URI.parse(url))
    puts JSON.parse(resp.body)
  rescue StandardError => e
    puts "Problem calling url: #{e.message}"
    break
  end
  if(resp.code != "404")
    "success"
    JSON.parse(resp.body)
  else
    "error"
  end
end
=end

other_code